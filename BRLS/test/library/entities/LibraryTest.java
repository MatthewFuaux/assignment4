package library.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import library.entities.helpers.BookHelper;
import library.entities.helpers.CalendarFileHelper;
import library.entities.helpers.IBookHelper;
import library.entities.helpers.ILoanHelper;
import library.entities.helpers.IPatronHelper;
import library.entities.helpers.LibraryFileHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;

class LibraryTest {
	static ILibrary library;
	static IBookHelper bookHelper;
	static IPatronHelper patronHelper;
	static ILoanHelper loanHelper;
	static ICalendar calendar;
	static LibraryFileHelper libraryHelper;
	static CalendarFileHelper calendarHelper;
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		bookHelper = new BookHelper();
		patronHelper = new PatronHelper();
		loanHelper = new LoanHelper();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		libraryHelper = new LibraryFileHelper(new BookHelper(), new PatronHelper(), new LoanHelper());
		calendarHelper = new CalendarFileHelper();
		calendar = calendarHelper.loadCalendar();
		library = libraryHelper.loadLibrary();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testBugOne() {
		IPatron patron = library.addPatron("Smith", "John", "jsmith@gmail.com", 612999555343L);
		IBook book = library.addBook("Author", "Title", "1234");
		ILoan loan = library.issueLoan(book, patron);
		library.commitLoan(loan);
		calendar.incrementDate(3);
		library.checkCurrentLoansOverDue();
		double fine = library.calculateOverDueFine(loan);
		assertTrue(fine==1.0);
		//Returns 0.0 when it should be 1.0
		//After changing MILLIS_PER_DAY this is fixed
	}
	@Test
	
	void testBugTwo() {
		IPatron patron = library.addPatron("Smith", "John", "jsmith@gmail.com", 612999555343L);
		IBook book = library.addBook("Author", "Title", "1234");
		ILoan loan = library.issueLoan(book, patron);
		library.commitLoan(loan);
		calendar.incrementDate(4);
		library.checkCurrentLoansOverDue();
		double fine = library.calculateOverDueFine(loan);
		assertTrue(fine==2.0);
		//Returns 1.0 when it should be 2.0
		//After changing MILLIS_PER_DAY this is fixed
	}

}
